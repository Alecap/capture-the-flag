<?php
	$alice_qubits = [];
	$alice_basis = [];

	for ($i=0; $i < 512; $i++) { 
		array_push($alice_qubits, array('real'=>rand(0,1), 'imag' => rand(0,1)));
		//Generate random quibits
		array_push($alice_basis, substr(str_shuffle("+x"), 0, 1));
		//Generate random basis
	}

	//CURL Request
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://cryptoqkd.web.ctfcompetition.com/qkd/qubits');
	
	$headers = [
		'Content-Type: application/json',
		'Content-Length: 512',
	];

	$data = array(
		'basis' => $alice_basis,
		'qubits' => $alice_qubits
	);

	$postfields = json_encode($data);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
	$response = curl_exec($ch);
	curl_close($ch);


	//measure($alice_qubits,$alice_basis);


	function measure(&$qubits, $basis)
	{
		for ($i=0; $i < sizeof($basis); $i++) { 
			$complex_number = new Complex($qubits[$i], $qubits[$i]);
			if ($basis[$i] == 'x') {
				$complex_number->multiply(0.707, -0.707);
			}
			$qubits[$i] = $complex_number;
		}
	}

	class Complex {
		public $real = 1;
		public $imaginary = 1;

		public function __construct($real = 1, $imaginary = 1) {
			$this->real = $real;
			$this->imaginary = $imaginary;
		}

		public function multiply($real = 1, $img = 1) {
			$this->real *= $real;
			$this->imaginary *= $img;
		}

		public function __toString() {
			return '('.$this->real.','.$this->imaginary.'j)';
		}

	}
	
?>